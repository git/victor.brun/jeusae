//==================================================================================================
// ANIMATION AVEC TYPESCRIPT                                                                 Jeu.ts
//==================================================================================================

// Classe  J e u //---------------------------------------------------------------------------------
class Jeu extends Scene {
  //----------------------------------------------------------------------------------------Attributs
  /* Declarer ici les attributs de la scene. */
  public carte_ : Array<Array<number>>;
  public wall_ : Sprite;
  public pas_ : number;
  public rat_ : Rat;
  public fromage_ : Sprite;
  public fraise_ : Sprite;
  public arriverx_ : number;
  public arrivery_ : number;
  public arriver_ : Sprite;
  public compteurfromage_ : number;
  public compteurfraise_ : number;
  public mangerfromage_ : Array<Array<Sprite>>;
  public mangerfraise_ : Array<Array<Sprite>>;
  public niveau_ : any;
  public idniveau_ : number;
 
 
 
  //-------------------------------------------------------------------------------------Constructeur
  public constructor(element : HTMLElement) {
   super(element,false);
   /* Ecrire ici le code qui initialise la scene. */
   this.pas_ = 32; 
   this.mangerfromage_ = [];
   this.mangerfraise_ = [];
   this.compteurfraise_ = 0;
   this.compteurfromage_ = 0;
   this.idniveau_ = 1;
  }
 
  private telechargerNiveau(id : number){
   let requete : XMLHttpRequest = new XMLHttpRequest();
   let parametres : string = "id=" + id;
   requete.open("get", "http://localhost/SAE/index.php?" + parametres);
   /* Ecouteur déclenché à la réception des données */
   requete.onreadystatechange = () => {
     if (requete.readyState == XMLHttpRequest.DONE && requete.status == 200) {
      let result = requete.responseText;
        if(result =="false"){
          console.log("fin du jeu");
          window.location.href = "jeufin.htm";
        }else{
          this.niveau_ = JSON.parse(requete.responseText);
          /* Suite du code */
          console.log(this.niveau_.id);
          console.log(this.niveau_.niveau);
          this.start_suite();
        }
      }
    }
   requete.send();
 }
 
 private initialiserCarte(){
  /* ancien code pour la carte
  this.carte_ = [];
   this.carte_[0] =  [1 ,1 ,1 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,1 ,1 ,1 ,1 ,1 ,0 ,0 ];
   this.carte_[1] =  [1 ,8 ,1 ,0 ,0 ,1 ,2 ,2 ,2 ,2 ,2 ,1 ,0 ,1 ,2 ,2 ,2 ,1 ,0 ,0 ];
   this.carte_[2] =  [1 ,2 ,1 ,0 ,0 ,1 ,2 ,4 ,4 ,4 ,2 ,1 ,0 ,1 ,2 ,1 ,2 ,1 ,1 ,0 ];
   this.carte_[3] =  [1 ,2 ,1 ,0 ,0 ,1 ,2 ,1 ,1 ,1 ,2 ,1 ,0 ,1 ,2 ,3 ,2 ,2 ,1 ,0 ];
   this.carte_[4] =  [1 ,2 ,1 ,1 ,1 ,1 ,2 ,2 ,2 ,1 ,2 ,1 ,0 ,1 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[5] =  [1 ,2 ,2 ,2 ,1 ,3 ,2 ,1 ,2 ,1 ,2 ,1 ,0 ,1 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[6] =  [1 ,1 ,1 ,2 ,1 ,2 ,4 ,1 ,2 ,1 ,2 ,1 ,0 ,1 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[7] =  [0 ,0 ,1 ,2 ,1 ,2 ,4 ,1 ,2 ,1 ,2 ,1 ,1 ,1 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[8] =  [1 ,1 ,1 ,2 ,1 ,2 ,4 ,1 ,2 ,2 ,2 ,2 ,1 ,4 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[9] =  [1 ,2 ,2 ,2 ,1 ,2 ,4 ,1 ,2 ,1 ,1 ,2 ,1 ,4 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[10] = [1 ,2 ,1 ,1 ,1 ,2 ,4 ,1 ,2 ,1 ,1 ,2 ,1 ,4 ,2 ,1 ,4 ,2 ,1 ,0 ];
   this.carte_[11] = [1 ,2 ,2 ,2 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,1 ,4 ,2 ,1 ,1 ];
   this.carte_[12] = [1 ,1 ,1 ,2 ,1 ,1 ,1 ,1 ,2 ,1 ,1 ,2 ,1 ,2 ,1 ,1 ,1 ,2 ,9 ,1 ];
   this.carte_[13] = [0 ,0 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,1 ,1 ,3 ,2 ,2 ,1 ,0 ,1 ,1 ,1 ,1 ];
   this.carte_[14] = [0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ];
   */
   this.carte_ = JSON.parse(this.niveau_.niveau);
 }
 
 public dessinerLabyrinthe(){
  // dessine la map et place les objets et le rat
   for(let i = 0 ; i<this.carte_.length; i++){
    // creation des tableaux pour le fromage et les fraises
     this.mangerfromage_[i] = [];
     this.mangerfraise_[i] = [];
     for(let j = 0 ; j<this.carte_[i].length; j++){
      // placement de la fin de la map sur la carte
       if (this.carte_[i][j] == 9){
         this.arriverx_ = j;
         this.arrivery_ = i;
         this.arriver_ = new Sprite(document.createElement("img"));
         this.arriver_.setImage("fin.png",32,32);
         this.arriver_.setXY(this.pas_*j,this.pas_*i);
         this.appendChild(this.arriver_);
       }
       // placement des murs sur la carte
       if(this.carte_[i][j] == 1){
         this.wall_ = new Sprite(document.createElement("img"));
         this.wall_.setImage("mur.jpg",32,32);
         this.wall_.setXY(this.pas_*j,this.pas_*i);
         this.appendChild(this.wall_);
       }
       // placement du rat sur la carte
       if (this.carte_[i][j] == 8){
         this.rat_ = new Rat(this,document.createElement("img"),i,j);
         this.rat_.setImage("rat.png",32,32);
         this.rat_.setXY(this.pas_*j,this.pas_*i);
         this.appendChild(this.rat_);
       }
       // placement des fromages sur la carte
       if (this.carte_[i][j] == 2){
         this.fromage_ = new Sprite(document.createElement("img"));
         this.fromage_.setImage("fromage.png",32,32);
         this.fromage_.setXY(this.pas_*j,this.pas_*i);
         this.appendChild(this.fromage_);
       }
       // placement des fraises sur la carte
       if (this.carte_[i][j] == 3){
         this.fraise_ = new Sprite(document.createElement("img"));
         this.fraise_.setImage("fraise.png",32,32);
         this.fraise_.setXY(this.pas_*j,this.pas_*i);
         this.appendChild(this.fraise_);
       }
       // remplis le tableau qui contient uniquement le fromage
       if(this.carte_[i][j] == 2){
         this.mangerfromage_[i][j] = this.fromage_;
       } else {
         this.mangerfromage_[i][j] = null;
       }
       // remplis le tableau qui contient uniquement les fraises
       if(this.carte_[i][j] == 3){
         this.mangerfraise_[i][j] = this.fraise_;
       } else {
         this.mangerfraise_[i][j] = null;
       }
     }
   }
 }

// retire le fromage de la map
 public retirerFromage(y : number, x : number){
   this.carte_[y][x] = 4; //place un numero 4 dans le tableau la ou le rat etais le tour d'avant
   this.mangerfromage_[y][x] = null; //remplace le fromage qui etais dans le tableau par nul
   for(let i = 0 ; i<this.carte_.length; i++){
     for(let j = 0 ; j<this.carte_[i].length; j++){
       if (this.carte_[i][j] == 8){
         this.carte_[i][j] = 4;
         this.compteurfromage_++; //augmente le compteur de fromage manger
         console.log(this.compteurfromage_); //pour debug et voir le nombre de fromage manger
       }
     }
   }
   this.removeAllChildNodes();
 }
 //la meme chose que le fromage mais pour les fraises
 public retirerFraise(y : number, x : number){
   this.carte_[y][x] = 4;
   this.mangerfraise_[y][x] = null;
   for(let i = 0 ; i<this.carte_.length; i++){
     for(let j = 0 ; j<this.carte_[i].length; j++){
       if (this.carte_[i][j] == 8){
         this.carte_[i][j] = 4;
         this.compteurfraise_++;
         console.log(this.compteurfraise_);
       }
     }
   }
   this.removeAllChildNodes();
 }
 
 //replace le personnage la ou il doit etre apres un deplacement
 public replacerPersonnage(ny : number, nx : number, ay : number, ax : number){
   this.carte_[ny][nx] = 8; //positionne le personnage au nouveau x et y
   this.carte_[ay][ax] = 4; //remplace l'ancienne position par le numero 4 
   console.log(ny,nx,ay,ax); //pour debug les problemes
   this.removeAllChildNodes();
 }
 
 //sert a enlever tout les elements de la scene pour que le tour s'actualise
 public removeAllChildNodes() {
   const scene = document.getElementById('scene');
   //le local storage sert a garder en memoire pour la page de fin
   localStorage.setItem("compteurfromage", this.compteurfromage_.toString());
   localStorage.setItem("compteurfraise", this.compteurfraise_.toString());
   while (scene.firstChild) {
       scene.removeChild(scene.firstChild);
   }
 }
 //sert a afficher sur la page de fin le nombre de fraise et fromage manger
 public fin(){
  const compteurfromage = localStorage.getItem("compteurfromage");
  const compteurfraise = localStorage.getItem("compteurfraise");
  console.log(compteurfromage);
  console.log(compteurfraise);
  const element = document.getElementById("compteurfromage");
    if (element) {
      element.textContent = "Fromage manger : " + compteurfromage;
    }
    const element2 = document.getElementById("compteurfraise");
    if (element2) {
      element2.textContent = "Fraise manger : " + compteurfraise;
    }
 }
 
  //--------------------------------------------------------------------------------------------start
  public start_suite() {
   /* Ecrire ici le code qui demarre la scene. */
   this.removeAllChildNodes(); //enleve tout les elements de la scene au cas ou
   console.log(this.carte_); //pour le debug de la carte
   this.initialiserCarte(); 
   this.dessinerLabyrinthe();
   this.rat_.animer(); //sert a bouger le personnage
   this.rat_.estArriver(); //pour savoir si le joueur a finis la map
  }
  public override start() {
   /* Ecrire ici le code qui demarre la scene. */
   //telecharge la carte qui est dans le backend sur une autre page
   this.telechargerNiveau(this.idniveau_);
   console.log(this); //pour debug l'initialisation du jeu
  }
 
  //--------------------------------------------------------------------------------------------pause
  public override pause() {
   /* Ecrire ici le code qui met la scene en pause. */
   this.rat_.figer();
  }
 
  //------------------------------------------------------------------------------------------unpause
  public override unpause() {
   /* Ecrire ici le code qui sort la scene de la pause. */
  }
 
  //--------------------------------------------------------------------------------------------clean
  public override clean() {
   /* Ecrire ici le code qui nettoie la scene en vue d'un redemarrage. */
 
  }
 }
 
 // Fin //-------------------------------------------------------------------------------------------
 