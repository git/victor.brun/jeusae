class Rat extends Sprite {

  //Attributs
  public scene_ : Jeu;
  private ecouteurDeplacer : any;
  public px_ : number;
  public py_ : number;
  public ancienpx_ : number;
  public ancienpy_ : number;
  //Constructeur
  public constructor(scene : Jeu,element : HTMLElement, py : number, px : number){
    super(element);
    this.scene_ = scene;
    this.px_ = px;
    this.py_ = py;
    this.ecouteurDeplacer = (event : KeyboardEvent) => {this.deplacer(event)};
    this.getElement().style.zIndex = "2"; // sert pour que le rat soit toujours au dessus
  }

  //sert a bouger vert le haut jusqu'au mur le plus proche avec un rafraichissement de 20ms
  async haut(){
    while(this.scene_.carte_[this.py_-1][this.px_] != 1){
      this.ancienpy_ = this.py_;
      this.ancienpx_ = this.px_;
      this.py_ = this.py_-1;
      this.setXY(this.getX(),this.getY()-this.scene_.pas_);
      this.manger();
      console.log("oui");
      await new Promise<void>(resolve => setTimeout(resolve, 20)); //attend 20ms entre chaque mouvement
    }
  }
  //sert a bouger vert le bas jusqu'au mur le plus proche avec un rafraichissement de 20ms
  async bas(){
    while(this.scene_.carte_[this.py_+1][this.px_] != 1){
      this.ancienpy_ = this.py_;
      this.ancienpx_ = this.px_;
      this.py_ = this.py_+1;
      this.setXY(this.getX(),this.getY()+this.scene_.pas_);
      this.manger();
      await new Promise<void>(resolve => setTimeout(resolve, 20)); //attend 20ms entre chaque mouvement
    }
  }
  //sert a bouger vert la gauche jusqu'au mur le plus proche avec un rafraichissement de 20ms
  async gauche(){
    while(this.scene_.carte_[this.py_][this.px_-1] != 1){
      this.ancienpy_ = this.py_;
      this.ancienpx_ = this.px_;
      this.px_ = this.px_-1;
      this.setXY(this.getX()-this.scene_.pas_,this.getY());
      this.manger();
      await new Promise<void>(resolve => setTimeout(resolve, 20)); //attend 20ms entre chaque mouvement
    }
  }
  //sert a bouger vert la droite jusqu'au mur le plus proche avec un rafraichissement de 20ms
  async droite(){
    while(this.scene_.carte_[this.py_][this.px_+1] != 1){
      this.ancienpy_ = this.py_;
      this.ancienpx_ = this.px_;
      this.px_ = this.px_+1;
      this.setXY(this.getX()+this.scene_.pas_,this.getY());
      this.manger();
      await new Promise<void>(resolve => setTimeout(resolve, 20)); //attend 20ms entre chaque mouvement
    }
  }
  // sert a deplacer le personnage et si il est arriver demande le prochain niveau
  private deplacer(event : KeyboardEvent){
    if(this.estArriver() == false){
      if(event.key == "ArrowLeft"){
      this.gauche();
      }
      if(event.key == "ArrowRight"){
        this.droite();
      }
      if(event.key == "ArrowDown"){
        this.bas();
      }
      if(event.key == "ArrowUp"){
        this.haut();
      }
    }else{
      this.scene_.idniveau_++;
      this.scene_.start();
    }
  }

  public manger(){
    //sert a bouger le personnage si la case est vide
    if(this.scene_.carte_[this.py_][this.px_] == 4 || this.scene_.carte_[this.py_][this.px_] == 9 ){
      this.scene_.replacerPersonnage(this.py_,this.px_,this.ancienpy_,this.ancienpx_);
      console.log("j'ai bouger");
    }else if(this.scene_.carte_[this.py_][this.px_] == 2){//sert a bouger le personnage si la case est un fromage
      this.scene_.retirerFromage(this.py_,this.px_);
      console.log("j'ai mange");
      this.scene_.carte_[this.py_][this.px_] = 8;
    }else if(this.scene_.carte_[this.py_][this.px_] == 3){//sert a bouger le personnage si la case est une fraise
      this.scene_.retirerFraise(this.py_,this.px_);
      console.log("j'ai mange");
      this.scene_.carte_[this.py_][this.px_] = 8;
    }
    this.scene_.dessinerLabyrinthe();
  }

  public animer(){
    // ajoute un ecouteur de si il y a une touche presser
    window.addEventListener("keydown", this.ecouteurDeplacer);
  }

  public figer(){
    // enleve l'ecouteur de la touche presser
    window.removeEventListener("keydown", this.ecouteurDeplacer);
  }

  public estArriver(){
    //savoir si le personnage est arriver si oui return un true 
    return this.scene_.carte_[this.scene_.arrivery_][this.scene_.arriverx_] == 8;
  }  
}